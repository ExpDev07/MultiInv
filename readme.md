# MultiInv (Multi Inventory)

MultiInv is a plugin developed by [ExpDev](https://expdev.eu/). A thread on
[SpigotMC by William029](https://www.spigotmc.org/threads/loading-inventories.250551/)
caught my attention. They wanted to know how to efficiently load and save inventories,
based on IDs and UUIDs. I had done this before, using JSON in my 
[InvRestore+ plugin](https://www.spigotmc.org/resources/22027/), but not using the Bukkit
Configuration Serialization, so I'd figured I give it a go. **This is the final product**.

### How to download
1. Put jar into /plugins/ folder.
2. Start server and let the folder "MultiInv" generate.
3. A file called "config.yml" will also generate. Here will all the inventories be stored.
4. Commands and permissions can be found [here](https://gitlab.com/ExpDev07/MultiInv/blob/master/src/main/resources/plugin.yml).

### Some info
Inventories will be hashed with something called [base64](http://www.unit-conversion.info/texttools/base64/).
So the "config.yml" file will look rather unfamiliar to you, as it will be filled with weird
long looking Strings. Manually inputting
inventories will be rather difficult and impossible. However, maybe I'll add in-game support
to edit inventories.

The plugin saves on disable and every 30 minutes, so remember to shutdown properly so
players don't lose their saves. Nobody wants for that to happen.

### Screenshots
The "config.yml" file. As you can see, one player has saved two inventories by IDs (test1
and test2)!

![config.yml file](https://i.gyazo.com/54c2a20e132c47e4f1e391d341421d7a.png)
https://gyazo.com/54c2a20e132c47e4f1e391d341421d7a

In game messages.

![in game messages](https://i.gyazo.com/bf060b4104affd546e8fba18708abf32.png)
https://gyazo.com/bf060b4104affd546e8fba18708abf32

### Happy using!
Hope (maybe?) this plugin comes to help. Will probably work more on it, however, I'm
not very fond of yml and would probably just create a completely new version.



