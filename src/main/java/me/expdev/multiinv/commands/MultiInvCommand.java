package me.expdev.multiinv.commands;

/*
 * Project created by ExpDev
 */


import me.expdev.multiinv.CInventory;
import me.expdev.multiinv.MultiInvPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;
import java.util.Map;

public class MultiInvCommand implements CommandExecutor {

    public static final String USAGE = ChatColor.RED + "Usage: /multiinv <get|save> <id>";

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(ChatColor.DARK_RED + "Only players!");
            return true;
        }

        // Player to get/save inventory for
        Player player = (Player) commandSender;

        if (args.length < 2) {
            player.sendMessage(USAGE);
            return true;
        }

        // Their UUID, String format
        String uuid = player.getUniqueId().toString();

        // Id to save inventory behind
        String id = args[1];

        // Inventory of player
        Inventory inventory = player.getInventory();

        // Map containing all our players
        Map<String, Map<String, CInventory>> playerMap = MultiInvPlugin.getPlayerMap();

        // Do we have any saves for the player?
        if (!playerMap.containsKey(uuid)) {
            // No, create one
            playerMap.put(uuid, new HashMap<>());
        }

        // Get all of player's saves
        Map<String, CInventory> saves = playerMap.get(uuid);

        if (args[0].equalsIgnoreCase("save")) {
            player.sendMessage(ChatColor.BLUE + "Saving your inventory as " + ChatColor.GREEN + "\"" + id + "\"" + ChatColor.BLUE + ".");

            // Save their inventory!
            saves.put(id, new CInventory(id, inventory));
        }

        // That's how to save a player's inventory, now how to get!
        // Normally, you'd go through checking if the map contains their uuid
        // etc, but we did that above so all we need to do is:
        else if (args[0].equalsIgnoreCase("get")) {

            // Making sure that the id exists!
            if (saves.containsKey(id)) {
                CInventory cInventory = saves.get(id);

                player.sendMessage(ChatColor.BLUE + "Giving your saved inventory " + ChatColor.GREEN + "(" + cInventory.getId() + ")" + ChatColor.BLUE + ".");

                // Set the contents and update
                player.getInventory().setContents(cInventory.getInventory().getContents());
                player.updateInventory();
            } else {
                player.sendMessage(ChatColor.GREEN + "\"" + id + "\"" + ChatColor.BLUE + " is not a valid save. Your valid saves are: ");
                for (String saveId : saves.keySet()) {
                    player.sendMessage(ChatColor.BLUE + "- " + ChatColor.GREEN + saveId);
                }
            }
        } else {
            player.sendMessage(USAGE);
            return true;
        }

        return true;
    }
}
