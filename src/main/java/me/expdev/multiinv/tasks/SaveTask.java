package me.expdev.multiinv.tasks;

import me.expdev.multiinv.CInventory;
import me.expdev.multiinv.MultiInvPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Map;

/*
 * Project created by ExpDev
 */

public class SaveTask extends BukkitRunnable {

    private static boolean running = false;

    @Override
    public void run() {
        running = true;

        Map<String, Map<String, CInventory>> playerMap = MultiInvPlugin.getPlayerMap();

        // Serializing and saving
        if (!playerMap.isEmpty()) {
            MultiInvPlugin.getInstance().getConfig().set("inventories", playerMap);
            MultiInvPlugin.getInstance().saveConfig();
        }

        running = false;
    }

    public static boolean isRunning() {
        return running;
    }
}
