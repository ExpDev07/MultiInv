package me.expdev.multiinv;

import me.expdev.multiinv.utils.InventoryUtils;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.inventory.Inventory;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

@SerializableAs("CInventory")
public class CInventory implements ConfigurationSerializable {

    private String id;
    private String base64Inventory;

    // Don't want to serialize this, transient is
    // used for readability
    private transient Inventory inventory;

    public CInventory(String id, String base64Inventory) {
        Objects.requireNonNull(id, "ID cannot be null!");
        Objects.requireNonNull(base64Inventory, "Base64Inventory cannot be null!");

        this.id = id;
        this.base64Inventory = base64Inventory;
    }

    public CInventory(String id, Inventory inventory) {
        // Important to not directly store the Inventory, but rather
        // initialize it, as keeping a reference to the player's
        // live Inventory can be rather dangerous :P
        this(id, InventoryUtils.inventoryToBase64(inventory));
    }

    public String getId() {
        return id;
    }

    private void initInventory() {
        // If it isn't null, then it has already been initialized
        if (inventory == null) {
            // Instead of doing this, you'd get the Inventory from the
            // String -> Inventory you're using
            try {
                this.inventory = InventoryUtils.inventoryFromBase64(base64Inventory);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public final Inventory getInventory() {
        // Initializing inventory before getting it
        initInventory();

        return inventory;
    }

    @Override
    public Map<String, Object> serialize() {
        LinkedHashMap result = new LinkedHashMap();

        result.put("id", id);
        result.put("base64Inventory", base64Inventory);

        return result;
    }

    public static CInventory deserialize(Map<String, Object> args) {

        String id = null;
        String base64Inventory = null;

        if(args.containsKey("id")) {
            id = ((String)args.get("id"));
        }

        if(args.containsKey("base64Inventory")) {
            base64Inventory = ((String)args.get("base64Inventory"));
        }

        return new CInventory(id, base64Inventory);
    }


}
