package me.expdev.multiinv;

import me.expdev.multiinv.commands.MultiInvCommand;
import me.expdev.multiinv.tasks.SaveTask;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

/*
 * Project created by ExpDev
 */

public class MultiInvPlugin extends JavaPlugin {

    private static MultiInvPlugin i;

    public static MultiInvPlugin getInstance() {
        return i;
    }

    private static Map<String, Map<String, CInventory>> playerMap = new ConcurrentSkipListMap<>(String.CASE_INSENSITIVE_ORDER);

    static {
        // Register our serialization!
        ConfigurationSerialization.registerClass(CInventory.class, "CInventory");
    }

    // Tasks related
    private Integer saveTask = null;

    @Override
    public void onEnable() {
        i = this;

        // Load configurations
        saveDefaultConfig();

        // Deserialize!
        if (getConfig().get("inventories") != null) {
            ConfigurationSection inventories = getConfig().getConfigurationSection("inventories");

            for (String uuid : inventories.getKeys(false)) {
                Map<String, CInventory> ids = new HashMap<>();

                for (String id : getConfig().getConfigurationSection("inventories." + uuid).getKeys(false)) {
                    ids.put(id, (CInventory) getConfig().get("inventories." + uuid + "." + id));
                }

                // Finally, put them in them in the map
                playerMap.put(uuid, ids);
            }
        }

        // Registering commands
        getCommand("multiinv").setExecutor(new MultiInvCommand());

        // Registering listeners
        PluginManager pm = Bukkit.getPluginManager();

        // Starting tasks
        if (saveTask == null) {
            long ticks = 20 * 60 * 30L; // Every 30 minutes
            this.saveTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new SaveTask(), ticks, ticks);
        }
    }

    @Override
    public void onDisable() {
        // Serialize!
        if (!playerMap.isEmpty()) {
            getConfig().set("inventories", playerMap);
            saveConfig();
        }

        // Stopping tasks
        if (saveTask != null) {
            getServer().getScheduler().cancelTask(saveTask);
            this.saveTask = null;
        }
    }

    public static Map<String, Map<String, CInventory>> getPlayerMap() {
        return playerMap;
    }

}
